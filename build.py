import re
import sys
import json
import glob
import subprocess

import yaml
import docker
import cerberus
import requests
import colorlog

import multiprocessing
from joblib import Parallel, delayed


logger = colorlog.getLogger()

handler = colorlog.StreamHandler()
handler.setFormatter(colorlog.ColoredFormatter(
    '%(log_color)s%(levelname)s: %(message)s'))
logger.addHandler(handler)
logger.setLevel('INFO')
colorlog.default_log_colors['INFO'] = 'blue'


WHITE_LIST_PIPES = [
    {'file': 'pipes/azure-aks-deploy.yml', 'valid': False, 'errors': ['1.0.2 version available. Current version is 1.0.1', 'pipes/azure-aks-deploy.yml yml field not equal']},
    {'file': 'pipes/azure-aks-helm-deploy.yml', 'valid': False, 'errors': ['1.0.2 version available. Current version is 1.0.1', 'pipes/azure-aks-helm-deploy.yml yml field not equal']},
    {'file': 'pipes/azure-arm-deploy.yml', 'valid': False, 'errors': ['pipes/azure-arm-deploy.yml yml field not equal']},
    {'file': 'pipes/azure-cli-run.yml', 'valid': False, 'errors': ['1.0.3 version available. Current version is 1.0.2', 'pipes/azure-cli-run.yml yml field not equal']},
    {'file': 'pipes/azure-functions-deploy.yml', 'valid': False, 'errors': ['1.0.2 version available. Current version is 1.0.1', 'pipes/azure-functions-deploy.yml yml field not equal']},
    {'file': 'pipes/azure-vm-linux-script-deploy.yml', 'valid': False, 'errors': ['pipes/azure-vm-linux-script-deploy.yml yml field not equal']},
    {'file': 'pipes/azure-vmss-linux-script-deploy.yml', 'valid': False, 'errors': ['1.0.2 version available. Current version is 1.0.1', 'pipes/azure-vmss-linux-script-deploy.yml yml field not equal']},
    {'file': 'pipes/azure-web-apps-containers-deploy.yml', 'valid': False, 'errors': ['1.0.2 version available. Current version is 1.0.1', 'pipes/azure-web-apps-containers-deploy.yml yml field not equal']},
    {'file': 'pipes/azure-web-apps-deploy.yml', 'valid': False, 'errors': ['1.0.3 version available. Current version is 1.0.2', 'pipes/azure-web-apps-deploy.yml yml field not equal']},
    {'file': 'pipes/ld-find-code-refs-pipe.yml', 'valid': False, 'errors': ['1.4.0 version available. Current version is 1.0.0', 'pipes/ld-find-code-refs-pipe.yml yml field not equal']},
    {'file': 'pipes/rollbar-notify.yml', 'valid': False, 'errors': ['pipes/rollbar-notify.yml yml field not equal']},
    {'file': 'pipes/rolloutio-flag-templates.yml', 'valid': False, 'errors': ['pipes/rolloutio-flag-templates.yml yml field not equal']},
    {'file': 'pipes/snyk-scan.yml', 'valid': False, 'errors': ['pipes/snyk-scan.yml yml field not equal']},
    {'file': 'pipes/sonatype-nexus-repository-publish.yml', 'valid': False, 'errors': ['pipes/sonatype-nexus-repository-publish.yml yml field not equal']},
    {'file': 'pipes/whitesource-scan.yml', 'valid': False, 'errors': ['1.3.0 version available. Current version is 1.1.0', 'pipes/whitesource-scan.yml yml field not equal', "pipes/whitesource-scan.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"]},
    {'file': 'pipes/google-app-engine-deploy.yml', 'valid': False, 'errors': ["pipes/google-app-engine-deploy.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"]},
    {'file': 'pipes/debricked-scan.yml', 'valid': False, 'errors': ['0.5.3 version available. Current version is 0.3.0']},
    {'file': 'pipes/google-gke-kubectl-run.yml', 'valid': False, 'errors': ["pipes/google-gke-kubectl-run.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"]},
    {'file': 'pipes/synopsys-detect.yml', 'valid': False, 'errors': ["pipes/synopsys-detect.yml repository not valid: pipe.yml not valid: {'image': ['Docker images larger than 1GB not supported']}"]},
]


class PipeHasError(Exception):
    pass


class RepositoryHasNoTagsError(Exception):
    pass


def docker_image(field, value, error):
    try:
        client = docker.from_env()
        image = client.images.pull(value)
        if int(image.attrs['Size']) > 1073741824:
            error(field, "Docker images larger than 1GB not supported")
    except docker.errors.ImageNotFound:
        error(field, "Docker image not found")


ALLOWED_CATEGORIES = ['Alerting', 'Artifact management', 'Code quality', 'Deployment', 'Feature flagging',
                      'Monitoring', 'Notifications', 'Security', 'Testing', 'Utilities', 'Workflow automation']

REMOTE_METADATA = cerberus.Validator({
    'name': {'type': 'string', 'required': True},
    'description': {'type': 'string', 'required': True},
    'image': {'type': 'string', 'required': True, 'validator': docker_image},
    'repository': {'type': 'string', 'required': True},
    'icon': {'type': 'string', 'required': False},
    'maintainer': {'type': 'string', 'required': True},
    'tags': {'type': 'list', 'required': True},
    'variables': {'type': 'list', 'required': False}
})

LOCAL_METADATA = cerberus.Validator({
    'name': {'type': 'string', 'required': True},
    'description': {'type': 'string', 'required': True},
    'category': {'type': 'string', 'required': True, 'allowed': ALLOWED_CATEGORIES},
    'logo': {'type': 'string', 'required': False},
    'repositoryPath': {'type': 'string', 'required': True},
    'version': {'type': 'string', 'required': True},
    'vendor': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True}
        },
        'required': False
    },
    'maintainer': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': True
    },
    'yml': {'type': 'string', 'required': False},
    'tags': {'type': 'list', 'required': False}
})

FINAL_METADATA = cerberus.Validator({
    'name': {'type': 'string', 'required': True},
    'description': {'type': 'string', 'required': True},
    'category': {'type': 'string', 'required': True, 'allowed': ALLOWED_CATEGORIES},
    'logo': {'type': 'string', 'required': True},
    'repositoryPath': {'type': 'string', 'required': True},
    'version': {'type': 'string', 'required': True},
    'vendor': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True}
        },
        'required': False
    },
    'maintainer': {
        'type': 'dict',
        'schema': {
            'name': {'type': 'string', 'required': True},
            'website': {'type': 'string', 'required': True},
            'email': {'type': 'string', 'required': False}
        },
        'required': True
    },
    'yml': {'type': 'string', 'required': True},
    'tags': {'type': 'list', 'required': True}
})


def get_latest_tag(repository_path):
    """
    Gets the latest tag of a repository (sort alphabetically)
    :param repository_path: Repository path (ex: account/repo)
    :return: Latest tag
    """
    REPOSITORY_TAGS_URL = f"https://api.bitbucket.org/2.0/repositories/{repository_path}/refs/tags?sort=-name&pagelen=100"
    request_tags = requests.get(REPOSITORY_TAGS_URL)
    tags_objects = request_tags.json()['values']
    tags = [tag['name'] for tag in tags_objects]

    if not tags:
        raise RepositoryHasNoTagsError(repository_path)

    return max(tags, key=parse_version)


def parse_version(version):
    return tuple(map(int, version.split('.')))


def get_repository_info(repository_path):
    """
    Retrieves repository information
    :param repository_path: Repository path (ex: account/repo)
    :return: JSON response of repository information
    """
    request_repo_info = requests.get(f"https://api.bitbucket.org/2.0/repositories/{repository_path}/")
    request_repo_info.raise_for_status()
    return request_repo_info.json()


def readme_exists(repository_path, version):
    """
    Determine if README.md file exists
    :param repository_path: Repository path (ex: account/repo)
    :param version: Reference (i.e. tag or commit)
    :return: True if exists, False otherwise
    """
    request = requests.head(f"https://bitbucket.org/{repository_path}/raw/{version}/README.md")
    return request.status_code == requests.codes.ok


def _extract_yml_definition(data):
    """
    Extracts the YAML definition
    :param data: Readme.md contents
    :return: code snippet (see tests for examples)
    """
    search_group = re.search(r"## YAML Definition(?:(?:.*?\n)*?)```yaml((?:.*?\n)*?)```$", data, re.MULTILINE)
    if search_group is None:
        return None
    code_snippet = (search_group.group(1)).lstrip()
    return code_snippet


def get_yml_definition(repository_path, version):
    """
    Determine if README.md file exists
    :param repository_path: Repository path (ex: account/repo)
    :param version: Reference (i.e. tag or commit)
    :return: True if exists, False otherwise
    """
    test = requests.get(f"https://bitbucket.org/{repository_path}/raw/{version}/README.md")
    test.encoding = "utf-8"
    yml_definition = _extract_yml_definition(test.text)
    return yml_definition


def get_pipe_metadata(repository_path, version):
    """
    Retrieves pipe metadata
    :param repository_path: Repository path (ex: account/repo)
    :param version: Reference (i.e. tag or commit)
    :return: YAML object
    """
    request = requests.get(f"https://bitbucket.org/{repository_path}/raw/{version}/pipe.yml")
    request.raise_for_status()
    return yaml.safe_load(request.text)


def validate(pipe_yml, infile):
    errors = []

    if not LOCAL_METADATA.validate(pipe_yml):
        errors.append(f"{infile} not valid: {str(LOCAL_METADATA.errors)}")

    repository_path = pipe_yml['repositoryPath']
    version = pipe_yml['version']

    # Fetch Repository information
    repo_info = get_repository_info(repository_path)

    if 'logo' not in pipe_yml:
        pipe_yml['logo'] = repo_info['links']['avatar']['href']

    # Get latest tag
    latest_tag = get_latest_tag(repository_path)
    if latest_tag != version:
        errors.append(f"{latest_tag} version available. Current version is {version}")

    # Fetch the README.md
    if not readme_exists(repository_path, version):
        errors.append(f"{infile} repository not valid: README.md not found")

    # TODO: check this until we make sure all code snippets are similar
    yml_definition = get_yml_definition(repository_path, version)
    if 'yml' in pipe_yml and yml_definition != pipe_yml['yml']:
        errors.append(f"{infile} yml field not equal")
    if 'yml' not in pipe_yml:
        pipe_yml['yml'] = yml_definition

    if yml_definition is None:
        errors.append(f"{infile} yml definition in README.md does not exist")

    # validate YAML file
    try:
        yaml.safe_load(pipe_yml['yml'])
    except yaml.YAMLError as exc:
        errors.append(f"{infile} yml definition in README.md is not valid")

    # Fetch the pipe.yml (metadata)
    try:
        remote_metadata_yml = get_pipe_metadata(repository_path, version)
        if not REMOTE_METADATA.validate(remote_metadata_yml):
            errors.append(f"{infile} repository not valid: pipe.yml not valid: {str(REMOTE_METADATA.errors)}")
    except Exception as error:
        logger.error(error)
        errors.append(f"{infile} repository not valid: pipe.yml not found")

    # add tags from the remote if local tags are empty
    if not pipe_yml.get('tags') and remote_metadata_yml['tags']:
        pipe_yml['tags'] = remote_metadata_yml['tags']

    if not FINAL_METADATA.validate(pipe_yml):
        errors.append(f"{infile} final metadata not valid: {str(FINAL_METADATA.errors)}")

    return errors


def process(infile):
    print(f"Start: {infile}")
    errors = []
    with open(infile, 'r') as stream:
        try:
            yaml_file = yaml.safe_load(stream)
            errors = validate(yaml_file, infile)
            # add file creation timestamp to display recently added category in pipes marketplace
            process = subprocess.Popen([f"git log --format=%aD {infile} | tail -1"], shell=True, stdout=subprocess.PIPE)
            yaml_file['timestamp'] = f"\"{process.communicate()[0]}\""
            return {
                'metadata': {
                    'file': infile,
                    'valid': len(errors) == 0,
                    'errors': errors
                },
                'data': yaml_file
            }
        except yaml.YAMLError as exc:
            print(exc)
        finally:
            print(f"Finish: {infile}. Success: " + str(len(errors) == 0))


def is_pipe_broken(pipe_metadata):
    if pipe_metadata['valid']:
        return False

    if pipe_metadata['file'] not in [p['file'] for p in WHITE_LIST_PIPES]:
        return True

    known_errors = [p['errors'] for p in WHITE_LIST_PIPES if p['file'] == pipe_metadata['file']][0]

    if all(error in known_errors for error in pipe_metadata['errors']):
        return False
    else:
        return True


def main():
    path = 'pipes/*.yml'
    num_cores = min(multiprocessing.cpu_count(), 8)

    if len(sys.argv) > 1:
        path = sys.argv[1]

    list_files = sorted(glob.glob(path))

    results = Parallel(n_jobs=num_cores)(delayed(process)(i) for i in list_files)

    data = [result['data'] for result in results]
    metadata = [result['metadata'] for result in results]
    broken_pipes_metadata = [md for md in metadata if is_pipe_broken(md)]

    for broken_pipe in broken_pipes_metadata:
        message = ''
        if broken_pipe['file'] in [p['file'] for p in WHITE_LIST_PIPES]:
            known_errors = [p for p in WHITE_LIST_PIPES if p['file'] == broken_pipe['file']][0]
            message = f"\nBut WHITE_LIST_PIPES has: {known_errors}"
        logger.error(f"{broken_pipe}{message}\n")

    # should fail build if a pipe has errors and not in the White List
    if broken_pipes_metadata:
        raise PipeHasError('Fix errors before deploy!')

    # TODO: do not generate json files who contains errors
    logger.info("Summary:")
    for item in metadata:
        if not item['valid']:
            logger.warning(f"{item}\n")
        else:
            logger.info(f"{item}\n")

    with open('pipes.json', 'w', encoding='utf8') as outfile:
        json.dump(data, outfile, ensure_ascii=False)


if __name__ == '__main__':
    main()

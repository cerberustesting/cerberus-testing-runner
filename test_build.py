import unittest
from build import _extract_yml_definition

readme = """# Bitbucket Pipelines Pipe: Trigger Bitbucket Pipelines build

Trigger Bitbucket Pipelines build

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
  - pipe: atlassian/pipelines-trigger-build:0.0.0
    variables:
      APP_PASSWORD: $APP_PASSWORD
      REPO: 'your-awesome-repo'
      # ACCOUNT: '<string>' # Optional
      # REF_TYPE: '<string>' # Optional
```
## Variables

```yaml
  - pipe: atlassian/pipelines-trigger-build:0.0.0
    variables:
      APP_PASSWORD: $APP_PASSWORD
      REPO: 'your-awesome-repo'
```
"""

yaml_definition = """- pipe: atlassian/pipelines-trigger-build:0.0.0
    variables:
      APP_PASSWORD: $APP_PASSWORD
      REPO: 'your-awesome-repo'
      # ACCOUNT: '<string>' # Optional
      # REF_TYPE: '<string>' # Optional
"""


class TestBuild(unittest.TestCase):

    def test_extract_yml_definition_is_correct(self):
        self.assertEqual(_extract_yml_definition(readme), yaml_definition)

    def test_extract_yml_definition_fails(self):
        self.assertEqual(_extract_yml_definition("""### YAML Definition"""), None)


if __name__ == '__main__':
    unittest.main()
